import { Injectable } from "@angular/core";
import { Observable, of } from 'rxjs';
import * as io from 'socket.io-client';
import {Message} from '../model/message';

@Injectable({
    providedIn:"root"
})
export class SocketService {
    private serverUrl = "http://localhost:5000";
    private socket;

    public sendMessage(message) {
        this.socket.emit('add-message', message);
    }

    public getMessages(){
        let observable = new Observable(observer => {
            this.socket = io(this.serverUrl);
            this.socket.on('message', (message: Message) => {
              observer.next(message);
            });
            return () => {
              this.socket.disconnect();
            };
          })
          return observable;
    }
}