import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppBootstrapModule } from './bootstrap/bootstrap.module';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ChatComponent } from './chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ChatComponent
  ],
  imports: [ BrowserModule, AppBootstrapModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
