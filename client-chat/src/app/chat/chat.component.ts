import { Component, OnInit, OnDestroy, HostListener, ViewChild, ElementRef  } from '@angular/core';

import {SocketService} from '../shared/socket.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit,OnDestroy {

  @ViewChild('chatInput') chatInput: ElementRef;

  public messages = [];
  public connection;
  public message;
  
  constructor(private socketService: SocketService) { console.log("costruttore chiamato"); }

  ngOnInit() {
    this.connection = this.socketService.getMessages().subscribe(message => {
      this.messages.push(message);
    })
  }

  ngOnDestroy() {
    this.connection.unsubscribe();
  }

  sendMessage() {
     this.socketService.sendMessage(this.message);
    this.message = '';
  }

  @HostListener('click')
  public autofocusInput() {
    this.chatInput.nativeElement.focus();
  }
}
